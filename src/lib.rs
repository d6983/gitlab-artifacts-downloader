use curl::easy::{Easy, List};
use io::Read;
use reqwest::header::{HeaderName, HeaderValue};
use serde::Deserialize;
use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fs;
use std::io;
use std::io::Write;
use std::path::Path;
use std::str;

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let http_response = match request_api(&config) {
        Err(err) => return Err(err),
        Ok(res) => res,
    };

    if let Err(err) = parse_http_error(&http_response) {
        return Err(Box::new(err));
    }

    let jobs: Vec<Job> = serde_json::from_str(&http_response)?;

    let job_id = match get_job_id(&config.job_name, jobs) {
        Err(err) => return Err(Box::new(err)),
        Ok(id) => id,
    };

    let artifact_url = format!(
        "{}/api/v4/projects/{}/jobs/{}/artifacts",
        config.base_url, config.project_id, job_id
    );

    let mut list = List::new();
    list.append(&format!("PRIVATE-TOKEN: {}", config.private_token))
        .unwrap();

    let mut easy = Easy::new();
    easy.url(&artifact_url).unwrap();
    easy.http_headers(list).unwrap();

    let mut response_headers = Vec::new();
    {
        let mut transfer = easy.transfer();
        transfer
            .header_function(|header| {
                response_headers.push(str::from_utf8(header).unwrap().to_string());
                true
            })
            .unwrap();
        transfer.perform().unwrap();
    }
    if response_headers[0].contains("404") {
        return Err(Box::new(CustomError {
            msg: format!(
                "no artifacts for job \"{}\" with id \"{}\"",
                config.job_name, job_id
            ),
        }));
    }
    if !response_headers[0].contains("200") {
        return Err(Box::new(CustomError {
            msg: "unknown response while downloading artifacts".to_string(),
        }));
    }

    let path = Path::new("artifacts.zip");
    let display = path.display();
    let mut file = match fs::File::create(&path) {
        Err(err) => panic!("couldn't create {}: {}", display, err),
        Ok(file) => file,
    };
    easy.write_function(move |data| {
        if let Err(err) = file.write_all(data) {
            panic!("couldn't write to {}: {}", display, err);
        };
        Ok(data.len())
    })
    .unwrap();
    easy.perform().unwrap();

    if !is_zip(&path) {
        return Err(Box::new(CustomError {
            msg: "artifacts.zip is no valid zip file".to_string(),
        }));
    }
    Ok(())
}

fn is_zip(path: &Path) -> bool {
    let mut file = fs::File::open(path).expect("could not open artifacts.zip for validation");
    let mut buffer = [0; 4];
    let bytes = file
        .read(&mut buffer[..])
        .expect("failed to read from artifacts.zip");
    matches!(&buffer[..bytes], &[80, 75, 3, 4])
}

fn request_api(config: &Config) -> Result<String, Box<dyn Error>> {
    let header_name: HeaderName = "PRIVATE-TOKEN".parse().unwrap();
    let header_value: HeaderValue = config.private_token.parse().unwrap();

    let http_client = reqwest::blocking::Client::new();
    Ok(http_client
        .get(&config.joblist_url)
        .header(&header_name, &header_value)
        .send()?
        .text()?)
}

fn parse_http_error(http_response: &str) -> Result<(), CustomError> {
    if http_response.contains("Token is expired") {
        return Err(CustomError {
            msg: "token is expired!".to_string(),
        });
    } else if http_response.contains("401 Unauthorized") {
        return Err(CustomError {
            msg: "api token not authorized!".to_string(),
        });
    } else if http_response.contains("404 Project Not Found") {
        return Err(CustomError {
            msg: "project id invalid!".to_string(),
        });
    }
    Ok(())
}

fn get_job_id(job_name: &str, jobs: Vec<Job>) -> Result<i32, CustomError> {
    let mut job_id: i32 = 0;
    for job in jobs.iter() {
        if job.status != *"success" || job.name != job_name {
            continue;
        }
        let tmp = &mut job_id;
        *tmp = job.id;
        break;
    }
    if job_id == 0 {
        return Err(CustomError {
            msg: format!("no job with name \"{}\"", job_name),
        });
    }
    Ok(job_id)
}

#[derive(Debug, PartialEq)]
pub struct Config {
    base_url: String,
    private_token: String,
    project_id: i32,
    job_name: String,
    joblist_url: String,
}
impl Config {
    pub fn new(vars: &HashMap<String, String>) -> Result<Config, &'static str> {
        let base_url = match vars.get("GITLAB_BASE_URL") {
            None => return Err("missing variable \"GITLAB_BASE_URL\""),
            Some(url) if url.is_empty() => return Err("missing variable \"GITLAB_BASE_URL\""),
            Some(url) => url.trim().to_string(),
        };
        let private_token = match vars.get("GITLAB_PRIVATE_TOKEN") {
            None => return Err("missing variable \"GITLAB_PRIVATE_TOKEN\""),
            Some(token) if token.is_empty() => {
                return Err("missing variable \"GITLAB_PRIVATE_TOKEN\"")
            }
            Some(token) => token.trim().to_string(),
        };
        let project_id = match vars.get("GITLAB_PROJECT_ID") {
            None => return Err("missing variable \"GITLAB_PROJECT_ID\""),
            Some(id) if id.is_empty() => return Err("missing variable \"GITLAB_PROJECT_ID\""),
            Some(id) => match id.trim().parse::<i32>() {
                Err(_) => return Err("invalid value for \"GITLAB_PROJECT_ID\""),
                Ok(id) => id,
            },
        };
        let job_name = match vars.get("GITLAB_JOB_NAME") {
            None => return Err("missing variable \"GITLAB_JOB_NAME\""),
            Some(name) if name.is_empty() => return Err("missing variable \"GITLAB_JOB_NAME\""),
            Some(name) => name.trim().to_string(),
        };
        let joblist_url = format!("{}/api/v4/projects/{}/jobs/", base_url, project_id);

        Ok(Config {
            base_url,
            private_token,
            project_id,
            job_name,
            joblist_url,
        })
    }
}

#[derive(Deserialize)]
struct Job {
    id: i32,
    status: String,
    name: String,
}
#[derive(Debug, PartialEq)]
struct CustomError {
    msg: String,
}
impl fmt::Display for CustomError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}
impl Error for CustomError {}

#[cfg(test)]
mod tests {
    use super::*;
    fn get_env() -> HashMap<String, String> {
        let mut env: HashMap<String, String> = HashMap::new();

        env.insert(String::from("GITLAB_BASE_URL"), String::from("url"));
        env.insert(String::from("GITLAB_PRIVATE_TOKEN"), String::from("token"));
        env.insert(String::from("GITLAB_PROJECT_ID"), String::from("123"));
        env.insert(String::from("GITLAB_JOB_NAME"), String::from("name"));

        env
    }

    #[test]
    fn no_url_in_config() {
        let mut vars: HashMap<String, String> = get_env();
        let url = vars
            .entry(String::from("GITLAB_BASE_URL"))
            .or_insert(String::from(""));

        *url = String::from("");

        assert_eq!(
            Config::new(&vars),
            Err("missing variable \"GITLAB_BASE_URL\"")
        );
        vars.remove("GITLAB_BASE_URL");
        assert_eq!(
            Config::new(&vars),
            Err("missing variable \"GITLAB_BASE_URL\"")
        );
    }

    #[test]
    fn no_token_in_config() {
        let mut vars: HashMap<String, String> = get_env();
        let url = vars
            .entry(String::from("GITLAB_PRIVATE_TOKEN"))
            .or_insert(String::from(""));

        *url = String::from("");

        assert_eq!(
            Config::new(&vars),
            Err("missing variable \"GITLAB_PRIVATE_TOKEN\"")
        );
        vars.remove("GITLAB_PRIVATE_TOKEN");
        assert_eq!(
            Config::new(&vars),
            Err("missing variable \"GITLAB_PRIVATE_TOKEN\"")
        );
    }

    #[test]
    fn no_id_in_config() {
        let mut vars: HashMap<String, String> = get_env();
        let url = vars
            .entry(String::from("GITLAB_PROJECT_ID"))
            .or_insert(String::from(""));

        *url = String::from("");

        assert_eq!(
            Config::new(&vars),
            Err("missing variable \"GITLAB_PROJECT_ID\"")
        );
        vars.remove("GITLAB_PROJECT_ID");
        assert_eq!(
            Config::new(&vars),
            Err("missing variable \"GITLAB_PROJECT_ID\"")
        );
    }

    #[test]
    fn id_in_config_invalid() {
        let mut vars: HashMap<String, String> = get_env();
        let url = vars
            .entry(String::from("GITLAB_PROJECT_ID"))
            .or_insert(String::from(""));

        *url = String::from("1k4");

        assert_eq!(
            Config::new(&vars),
            Err("invalid value for \"GITLAB_PROJECT_ID\"")
        );
    }

    #[test]
    fn no_job_name_in_config() {
        let mut vars: HashMap<String, String> = get_env();
        let url = vars
            .entry(String::from("GITLAB_JOB_NAME"))
            .or_insert(String::from(""));

        *url = String::from("");

        assert_eq!(
            Config::new(&vars),
            Err("missing variable \"GITLAB_JOB_NAME\"")
        );
        vars.remove("GITLAB_JOB_NAME");
        assert_eq!(
            Config::new(&vars),
            Err("missing variable \"GITLAB_JOB_NAME\"")
        );
    }

    #[test]
    fn test_parse_http_error() {
        let expired = "Token is expired";
        let unauthorized = "401 Unauthorized";
        let not_found = "404 Project Not Found";
        assert_eq!(
            parse_http_error(expired),
            Err(CustomError {
                msg: format!("token is expired!")
            })
        );
        assert_eq!(
            parse_http_error(unauthorized),
            Err(CustomError {
                msg: format!("api token not authorized!")
            })
        );
        assert_eq!(
            parse_http_error(not_found),
            Err(CustomError {
                msg: format!("project id invalid!")
            })
        );
    }
    #[test]
    fn no_job_found() {
        let jobs: Vec<Job> = vec![
            Job {
                id: 5,
                status: "failure".to_string(),
                name: "correct".to_string(),
            },
            Job {
                id: 123,
                status: "success".to_string(),
                name: "incorrect".to_string(),
            },
        ];
        assert_eq!(
            get_job_id("correct", jobs),
            Err(CustomError {
                msg: format!("no job with name \"{}\"", "correct")
            })
        );
    }

    #[test]
    fn job_found() {
        let jobs: Vec<Job> = vec![
            Job {
                id: 5,
                status: "failure".to_string(),
                name: "correct".to_string(),
            },
            Job {
                id: 123,
                status: "success".to_string(),
                name: "incorrect".to_string(),
            },
            Job {
                id: 256,
                status: "success".to_string(),
                name: "correct".to_string(),
            },
        ];
        assert_eq!(get_job_id("correct", jobs), Ok(256));
    }

    #[test]
    #[allow(unused_must_use)]
    fn verify_zip_file() {
        let zip_path = Path::new("zip.zip");
        let zip = [0x50, 0x4b, 0x03, 0x04];
        fs::write(&zip_path, &zip[..]);
        assert!(is_zip(&zip_path));
        fs::remove_file(&zip_path);

        let nozip_path = Path::new("nozip.zip");
        let nozip = [0x50, 0x4b, 0x10, 0x04];
        fs::write(&nozip_path, &nozip[..]);
        assert!(!is_zip(&nozip_path));
        fs::remove_file(&nozip_path);
    }
}
