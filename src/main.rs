extern crate dotenv;
use dotenv::dotenv;
use gad::Config;
use gitlab_artifacts_downloader as gad;
use std::{env, process};

fn main() {
    dotenv().ok();

    let vars = env::vars().collect();
    let config = Config::new(&vars).unwrap_or_else(|err| {
        eprintln!("\nProblem loading config: {}", err);
        process::exit(1);
    });

    if let Err(err) = gad::run(config) {
        eprintln!("\nError while downloading artifacts: {}", err);
        process::exit(1);
    }
}
