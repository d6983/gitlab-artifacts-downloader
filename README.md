# Gitlab Artifacts Downloader

This programm aims to download production data provided by gitlab-ci job artifacts (if configured in the projects '.gitlab-ci.yml').
This is useful for local development.

## Getting started
### 1. Create your local .env file

1. Copy the '.env.sample' to '.env'
2. Sign in to GitLab.
3. In the upper-right corner, click your avatar and select Settings.
4. On the User Settings menu, select Access Tokens.
5. Choose a name and optional expiry date for the token.
6. Choose 'read api'.
7. Click the Create personal access token button.
8. Save the personal access token to you .env file. If you navigate away or refresh your page, and you did not save the token, you must create a new one.
9. Navigate to the respective project overview and copy the project id (From under the project name)
10. Click "CI/CD" on the side panel and then on "jobs".
11. Look for the name of the job, which provides downloadable production data.
12. Copy the name to your local '.env' file

### 2. Run the program

1. Clone the repository
2. Install rust
    ```bash
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    ```
3. Execute the compiler and run with:
    ```bash
    cargo run
    ```
4. If successful, your assets will be saved to 'artifacts.zip'
5. If you do not want to have Rust installed on your system, you can use the binary to run the program and uninstall rust:
   ```bash
   cargo build --release
   cp target/release/gitlab-artifacts-downloader .
   rustup self uninstall
   ```